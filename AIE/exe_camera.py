import cv2
from def_PosePredictor import PosePredictor
import numpy as np
import json
import time

# Get first frame
vc = cv2.VideoCapture(0)
rval, frame = vc.read()

font = cv2.FONT_HERSHEY_SIMPLEX
text_lines = ["Press ESC to stop", "Press SPACE to start"]

# Set starting position of text
x, y0 = 10, 30
dy = 30  # vertical space between lines

# Set recording flag
recording = False

# Initialize PosePredictor
predictor = PosePredictor(run_on_cpu=True)
verts = []
start_time = 0


while rval:    
    # Capture frame
    rval, frame = vc.read()
    
    # Do pose-estimation stuff
    if recording:
        verts.append(predictor.predict(frame))
        # draw indicator dots
        frame = predictor.draw_dots(frame)
    
    # horizontally flip frame to mirror display in window    
    frame = cv2.flip(frame, 1)
    
    # Add text prompt to frame
    for i, line in enumerate(text_lines):
        y = y0 + i*dy  # calculate y position for the current line
        cv2.putText(frame, line, (x, y), font, 1, (255, 255, 255), 2, cv2.LINE_AA)
    
    # frame delay (20 milliseconds)
    key = cv2.waitKey(20)
    
    # show frame to window
    cv2.imshow("Recording Animation", frame)
    
    # Check for key presses
    if key == 27: # if key == ESC, exit program
        break
    if key == 32: # if key == SPACE, start recording
        text_lines = ["Recording...", "Press ESC to stop"]
        recording = True
        start_time = time.time()


vc.release()
cv2.destroyWindow("Recording Animation")


verts = np.array(verts)
vertices_list = verts.tolist()
json_data = json.dumps(vertices_list, indent=4)
with open('vertices.json', 'w') as f:
    json.dump(vertices_list, f)
