from PoseEstimation3d import PoseEstimation3d
import sys

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python main.py <video_path> <method>")
        sys.exit(1)

    video_path = sys.argv[1]
    method = sys.argv[2]

    model = PoseEstimation3d()
    result = model.pose_estimation_on_video(video_path, method)