import argparse
import numpy as np
import cv2
import onnx
import onnxruntime as ort
import numpy as np
from PIL import Image
from pathlib import Path
import math

class PosePredictor:
    vertex = []
    
    # Define Variables, load model, and run session in initializer
    def __init__(self, run_on_cpu = True, quantized_model_path = "models/heavy/pose_landmark_heavy.onnx"):

        if (run_on_cpu):
            providers=['CPUExecutionProvider']
            provider_options=[{}]
        else:
            providers=['VitisAIExecutionProvider']
            cache_dir = Path(__file__).parent.resolve()
            provider_options = [{
                        'config_file': 'vaip_config.json',
                        'cacheDir': str(cache_dir),
                        'cacheKey': 'modelcachekey'
                    }]

        self.model = onnx.load(quantized_model_path)
        self.session = ort.InferenceSession(self.model.SerializeToString(), providers=providers,
                                    provider_options=provider_options)

    def predict(self, frame):
        height, width, channels = frame.shape
        x_scale = width/height
        frame = cv2.resize(frame, (256, 256))
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame = frame.astype(np.float32) / 255.0
        frame = np.expand_dims(frame, axis=0)  # Add batch dimension
        #run inference on frame
        input_details = self.session.get_inputs()[0]  # Assuming a single input for simplicity
        input_name = input_details.name
        output_details = self.session.get_outputs()[0]  # Assuming a single output for simplicity
        output_name = output_details.name
        output = self.session.run([output_name], {input_name: frame})

        # vertices is a list of 39 5-dimensional vectors
        vertices = output[0]
        # reshape to 39 3-dimensional vectors
        vertices = vertices.reshape(39, 5)

        # index 3, 4 are visibility and presence, delete them and adjust scale
        vertices[:, 0] = vertices[:, 0] * width / 256
        vertices[:, 1] = vertices[:, 1] * height / 256
        self.vertex = vertices
        
        return self.vertex

    # this is mediapipe's drawing function
    def draw_predicted_dots_on_image(self, frame):
        for vertex in self.vertex:
            # calculate the probablity of the given vertex is in screen
            sigmoid = 1 / (1 + math.exp(-vertex[3]))
            if sigmoid > 0.8:
                cv2.circle(frame, (int(vertex[0]), int(vertex[1])), 3, (0, 0, 255), -1)
        return frame

