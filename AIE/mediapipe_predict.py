#!/bin/python3

import argparse
import numpy as np
import cv2
import onnx
import onnxruntime as ort
import numpy as np
from PIL import Image
from pathlib import Path
import json

# vc = cv2.VideoCapture(0)
# rval, frame = vc.read()
# vc.release()

# boolean of testing on CPU or Vitis AI
run_on_cpu = False

if (run_on_cpu):
    providers=['CPUExecutionProvider']
    provider_options=[{}]
else:
    providers=['VitisAIExecutionProvider']
    cache_dir = Path(__file__).parent.resolve()
    provider_options = [{
                'config_file': 'vaip_config.json',
                'cacheDir': str(cache_dir),
                'cacheKey': 'modelcachekey'
            }]



quantized_model_path = "models/full/pose_landmark_full.onnx"
# quantized_model_path = "models/a2j/itop_side_quantized.onnx"

model = onnx.load(quantized_model_path)

session = ort.InferenceSession(model.SerializeToString(), providers=providers,
                               provider_options=provider_options)

# ==================================== PRINT INPUT AND OUTPUT INFORMATION ====================================
# Get input information
input_details = session.get_inputs()[0]  # Assuming a single input for simplicity
input_name = input_details.name
input_shape = input_details.shape
input_type = input_details.type

print(f"Input Name: {input_name}")
print(f"Input Shape: {input_shape}")
print(f"Input Type: {input_type}")

# Get output information
output_details = session.get_outputs()[0]  # Assuming a single output for simplicity
output_name = output_details.name
output_shape = output_details.shape
output_type = output_details.type

print(f"Output Name: {output_name}")
print(f"Output Shape: {output_shape}")
print(f"Output Type: {output_type}")

'''
# ==================================== RUN INFERENCE ON VIDEO ====================================
video_path = r'../../videos/curry_shooting.mp4'
cap = cv2.VideoCapture(video_path)

while cap.isOpened():
    ret, frame = cap.read()
    if not ret:
        break  # Video is finished

    # Preprocess the frame (assuming the same preprocessing as the single image example)
    input_size = (256, 256)  # Adjust based on your model's expected input size
    frame = cv2.resize(frame, input_size)
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    frame = frame.astype(np.float32) / 255.0
    frame = np.expand_dims(frame, axis=0)  # Add batch dimension

    # Run inference
    input_details = session.get_inputs()[0]  # Assuming a single input for simplicity
    input_name = input_details.name
    output_details = session.get_outputs()[0]  # Assuming a single output for simplicity
    output_name = output_details.name
    output = session.run([output_name], {input_name: frame})

    # Process the output here
    # For example, you might visualize the results on the frame
    # Note: This step depends on what your model predicts and how you want to use those predictions

# Release the video capture object
cap.release()
'''

# ==================================== RUN INTERENCE ON IMAGE ====================================
# open ohtani.jpg
image = Image.open(r'images/ohtani.jpg')
# convert image to numpy array
image = np.array(image)
# Get the dimensions of the image
height, width, channels = image.shape
print(f"Image Dimensions - Height: {height}, Width: {width}, Channels: {channels}")
x_scale = width/height

# print the shape of output: (batch_size, num_classes)
#resize image to (256, 256)
image = cv2.resize(image, (256, 256))
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
image = image.astype(np.float32) / 255.0
image = np.expand_dims(image, axis=0)  # Add batch dimension
#run inference on image
input_details = session.get_inputs()[0]  # Assuming a single input for simplicity
input_name = input_details.name
output_details = session.get_outputs()[0]  # Assuming a single output for simplicity
output_name = output_details.name
output = session.run([output_name], {input_name: image})

# vertices is a list of 39 5-dimensional vectors
vertices = output[0]
# reshape to 39 3-dimensional vectors
vertices = vertices.reshape(39, 5)
print(vertices)

# index 3, 4 are visibility and presence, delete them and adjust scale
vertices = vertices[:, 0:3]
vertices[:, 0] = vertices[:, 0] * x_scale
print(vertices)

# make vertices into a JSON file
vertices_list = vertices.tolist()
json_data = json.dumps(vertices_list, indent=4)
with open('vertices.json', 'w') as f:
    json.dump(vertices_list, f)
