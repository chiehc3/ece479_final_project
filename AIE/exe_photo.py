import cv2
import numpy as np
import json
# from tkinter import *
import sys
from PIL import Image
# from threading import Thread
from def_PosePredictor import PosePredictor



def play_video_with_predictor(video_path, predictor):
    cap = cv2.VideoCapture(video_path)
    verts = []
    
    if not cap.isOpened():
        print("Error: Could not open video.")
        return

    while True:
        rval, frame = cap.read()
        if not rval:
            break
        
        verts.append(predictor.predict(frame))
        frame = predictor.draw_predicted_dots_on_image(frame)
        
        cv2.imshow("Recording Animation", frame)
        if cv2.waitKey(33) == ord('q'):
            break
    
    cap.release()
    cv2.destroyAllWindows()

    verts_array = np.array(verts)
    vertices_list = verts_array.tolist()
    json_data = json.dumps(vertices_list, indent=4)
    with open('vertices.json', 'w') as f:
        json.dump(vertices_list, f)

    print(len(verts_array))


# Initialize PosePredictor
mediapipe_predictor = PosePredictor(run_on_cpu=True, quantized_model_path=r'models/lite/pose_landmark_lite.onnx')

image = Image.open(r'images/ohtani.jpg')
# convert image to numpy array
image = np.array(image)

prediction = mediapipe_predictor.predict(image)

image = mediapipe_predictor.draw_predicted_dots_on_image(image)

verts = np.array(prediction)
vertices_list = verts.tolist()
json_data = json.dumps(vertices_list, indent=4)
with open('vertices.json', 'w') as f:
    json.dump(vertices_list, f)

while True:
    cv2.imshow("Photo", image)
    if cv2.waitKey(33) == ord('q'):
        break


