import cv2
import numpy as np
import json
import sys
import os

os.environ['TF_ENABLE_ONEDNN_OPTS'] = '0'
import mediapipe as mp

dir = os.path.dirname(bpy.data.filepath)
if not dir in sys.path:
    sys.path.append(dir)
    
import def_PosePredictor 
import lib_rotation

import imp
imp.reload(def_PosePredictor)
imp.reload(lib_rotation)

from lib_rotation import *
from def_PosePredictor import PosePredictor

# =============================================== CODE STARTS HERE ===============================================

def play_video_with_predictor(video_path, predictor):
    cap = cv2.VideoCapture(video_path)
    verts = []
    frame_number = 0
    
    if not cap.isOpened():
        print("Error: Could not open video.")
        return

    while True:
        rval, frame = cap.read()
        if not rval:
            break
        
        # predict from image
        prediction = predictor.predict(frame)
        
        # add prediction to list
        verts.append(prediction)
        
        # draw indicator dots
        frame = predictor.draw_predicted_dots_on_image(frame)
        
        # set rotation for all bones in frame
        set_rotation_for_all_bones_in_frame(frame_number, prediction)
        frame_number += 1
        
        cv2.imshow("Recording Animation", frame)
        if cv2.waitKey(33) == ord('q'):
            break
    
    cap.release()
    cv2.destroyAllWindows()

    verts_array = np.array(verts)
    vertices_list = verts_array.tolist()
    json_data = json.dumps(vertices_list, indent=4)
    with open('vertices.json', 'w') as f:
        json.dump(vertices_list, f)

    print(len(verts_array))

def play_video_with_mediapipe_API(video_path):
    cap = cv2.VideoCapture(video_path)
    verts = []
    frame_number = 0
    
    mp_pose = mp.solutions.pose
    pose = mp_pose.Pose()
    
    if not cap.isOpened():
        print("Error: Could not open video.")
        return

    while True:
        rval, frame = cap.read()
        if not rval:
            break
        
        # preprocess image
        image_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        
        # predict from image
        results = pose.process(image_rgb)
        
        verts.append(results.pose_landmarks.landmark)
        
        # draw indicator dots
        frame = mp.solutions.drawing_utils.draw_landmarks(frame, results.pose_landmarks, mp_pose.POSE_CONNECTIONS)
        
        frame_number += 1
        
        cv2.imshow("Recording Animation", frame)
        if cv2.waitKey(33) == ord('q'):
            break
    
    cap.release()
    cv2.destroyAllWindows()
    pose.close()

    # verts_array = np.array(verts)
    # vertices_list = verts_array.tolist()
    # json_data = json.dumps(vertices_list, indent=4)
    # with open('vertices.json', 'w') as f:
    #     json.dump(vertices_list, f)

    # print(len(verts_array))

# Set video path
video_path = '../videos/curry_shooting.mp4'

# Initialize PosePredictor
# mediapipe_predictor = PosePredictor(run_on_cpu=True)

# play video with predictor
# play_video_with_predictor(video_path, mediapipe_predictor)
play_video_with_mediapipe_API(video_path)
