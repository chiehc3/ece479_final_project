import cv2
import numpy as np
import json
# from tkinter import *
import sys
# from threading import Thread
from def_PosePredictor import PosePredictor


sys.path.append('../blender_files')
from lib_rotation import *


def play_video_with_predictor(video_path, predictor):
    cap = cv2.VideoCapture(video_path)
    verts = []
    
    if not cap.isOpened():
        print("Error: Could not open video.")
        return

    while True:
        rval, frame = cap.read()
        if not rval:
            break
        
        verts.append(predictor.predict(frame))
        frame = predictor.draw_predicted_dots_on_image(frame)
        
        cv2.imshow("Recording Animation", frame)
        if cv2.waitKey(33) == ord('q'):
            break
    
    cap.release()
    cv2.destroyAllWindows()

    verts_array = np.array(verts)
    vertices_list = verts_array.tolist()
    json_data = json.dumps(vertices_list, indent=4)
    with open('vertices.json', 'w') as f:
        json.dump(vertices_list, f)

    print(len(verts_array))

# Set video path
video_path = '../videos/curry_shooting.mp4'

# Initialize PosePredictor
mediapipe_predictor = PosePredictor(run_on_cpu=True)

# # Initialize GUI
# root = Tk()
# root.title("Video Player")

# # Button to play video
# play_button = Button(root, text="Play Video", command=lambda: Thread(target=play_video_with_predictor, args=(video_path, mediapipe_predictor)).start())
# play_button.pack()

# root.mainloop()