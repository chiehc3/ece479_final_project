from mmpose.apis import MMPoseInferencer
from mediapipe import Image, ImageFormat
from mediapipe.tasks import python
from mediapipe.tasks.python import vision
import mmengine

import numpy as np
import time
import cv2
from tqdm import tqdm
import json
import os

class PoseEstimation3d:

    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(PoseEstimation3d, cls).__new__(cls)
        return cls._instance

    def __init__(self, device="cuda:0"):
        if not hasattr(self, 'initialized'):
            # Initialize here (expensive operations)
            self.initialized = True
            self.motionbert_inferencer = MMPoseInferencer(pose3d="motionbert_dstformer-ft-243frm_8xb32-120e_h36m", device=device)

            base_options = python.BaseOptions(
                model_asset_path='pose_landmarker.task',
            )
            options = vision.PoseLandmarkerOptions(
                base_options=base_options,
                output_segmentation_masks=True)

            self.mediapipe_detector = vision.PoseLandmarker.create_from_options(options)


    def estimate_pose_motionbert(self, image, time_inference):
        if time_inference:
            start_time = time.time()
            result = next(self.motionbert_inferencer(image))
            end_time = time.time()
            # print(f"MotionBert Time taken: {time.time() - start_time} seconds")
        else:
            result = next(self.motionbert_inferencer(image))

        if time_inference:
            return np.hstack((result['predictions'][0][0]['keypoints'], np.array(result['predictions'][0][0]['keypoint_scores']).reshape(-1, 1))), end_time - start_time
        else:
            return np.hstack((result['predictions'][0][0]['keypoints'], np.array(result['predictions'][0][0]['keypoint_scores']).reshape(-1, 1)))

    def estimate_pose_mediapipe(self, image, time_inference):
        if time_inference:
            start_time = time.time()
            detection_result = self.mediapipe_detector.detect(Image(image_format=ImageFormat.SRGB, data=image))
            end_time = time.time()
            # print(f"MediaPipe Time taken: {time.time() - start_time} seconds")
        else:
            detection_result = self.mediapipe_detector.detect(Image(image_format=ImageFormat.SRGB, data=image))
        if len(detection_result.pose_landmarks) == 0:
            return None  
        detection_result = [[item.x, item.y, item.z, item.presence] for item in detection_result.pose_landmarks[0]]
        
        if time_inference:
            return np.array(detection_result), end_time - start_time
        else:
            return np.array(detection_result)
        
    def estimate_pose(self, image, method="motionbert", time_inference=True):
        if method == "motionbert":
            return self.estimate_pose_motionbert(image, time_inference)
        elif method == "mediapipe":
            return self.estimate_pose_mediapipe(image, time_inference)
        else:
            raise ValueError("Invalid method")

    def read_video_frames(self, video_path):
        """
        Generator function that reads a video from the given path and yields each frame as a cv2 image.

        Args:
        video_path (str): Path to the video file.

        Yields:
        numpy.ndarray: The next frame of the video as a cv2 image.
        """
        # Open the video file
        if video_path.endswith('.mov'):
            cap = cv2.VideoCapture(video_path, cv2.CAP_FFMPEG)
        else:
            cap = cv2.VideoCapture(video_path)

        if not cap.isOpened():
            raise IOError(f"Cannot open video {video_path}")

        # Get total number of frames in the video
        total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

        # Read and yield each frame of the video with tqdm for progress tracking
        for _ in tqdm(range(total_frames), desc="Reading video frames"):
            ret, frame = cap.read()
            if not ret:
                break
            yield frame

        # Release the video capture object
        cap.release()

    def pose_estimation_on_video(self, video_path, method, save=True, time_inference=False):
        result = {}
        keypoints = []
        
        if method == "mediapipe":
            start_time = time.time()
            for frame in self.read_video_frames(video_path):
                output = self.estimate_pose(frame, method=method, time_inference=time_inference)
                if output is not None:
                    if time_inference:
                        keypoints.append(output[0].tolist())
                    else:
                        keypoints.append(output.tolist())

        elif method == "motionbert":
            frames = self.read_video_frames(video_path)
            generator = self.motionbert_inferencer(frames)
            start_time = time.time()
            for result in generator:
                keypoints.append(result['predictions'][0][0]['keypoints'])
        end_time = time.time()

        cap = cv2.VideoCapture(video_path)
        width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        cap.release()
        result['frame_info'] = {
            'width': width,
            'height': height,
            'total_frames': total_frames
        }

        result["keypoints"] = keypoints
        # Extract the filename without extension
        video_filename = os.path.splitext(os.path.basename(video_path))[0]
        output_path = f"./output/{method}/{video_filename}.json"
        os.makedirs(os.path.dirname(output_path), exist_ok=True)

        # Save the result as a JSON file
        with open(output_path, 'w') as f:
            json.dump(result, f)
        print(f"Results saved to {output_path}")
        if time_inference:
            print(f"{method}: Average time per frame: {(end_time - start_time) / total_frames * 1000} milliseconds")
        return result
    

model = PoseEstimation3d()

