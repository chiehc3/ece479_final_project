import bpy
import cv2
import numpy as np
import json
import sys
import os
import time

dir = os.path.dirname(bpy.data.filepath)
if not dir in sys.path:
    sys.path.append(dir)
    
import def_PosePredictor 
import lib_rotation

import imp
imp.reload(def_PosePredictor)
imp.reload(lib_rotation)

from lib_rotation import *
from def_PosePredictor import PosePredictor

# =============================================== CODE STARTS HERE ===============================================

def MP_ONNX_play_video_with_predictor(video_path):
    predictor = PosePredictor(run_on_cpu=True)
    
    cap = cv2.VideoCapture(video_path)
    verts = []
    frame_number = 0
    
    if not cap.isOpened():
        print("Error: Could not open video.")
        return

    while True:
        rval, frame = cap.read()
        if not rval:
            break
        
        # predict from image
        prediction = predictor.predict(frame)
        
        # add prediction to list
        verts.append(prediction)
        
        # draw indicator dots
        frame = predictor.draw_predicted_dots_on_image(frame)
        
        # set rotation for all bones in frame
        MP_set_rotation_for_all_bones_in_frame(frame_number, prediction)
        frame_number += 1
        
        cv2.imshow("Recording Animation", frame)
        if cv2.waitKey(33) == ord('q'):
            break
    
    cap.release()
    cv2.destroyAllWindows()

    verts_array = np.array(verts)
    vertices_list = verts_array.tolist()
    json_data = json.dumps(vertices_list, indent=4)
    with open('vertices.json', 'w') as f:
        json.dump(vertices_list, f)

    print(len(verts_array))

def MP_PRERUN_play_video_with_predictor(data, video_path):  
   
    cap = cv2.VideoCapture(video_path)
   
    for frame_number in range(len(data)):
        rval, frame = cap.read()
        if not rval:
            break
        # predict from image
        prediction = data[frame_number]
        
        for point in prediction:
            cv2.circle(frame, (int(point[0]), int(point[1])), 3, (0, 255, 0), -1)
            print(point)

        
        # set rotation for all bones in frame
        MP_set_rotation_for_all_bones_in_frame(frame_number, prediction)
        
        cv2.imshow("Recording Animation", frame)
        if cv2.waitKey(33) == ord('q'):
            break
        
        # time.sleep(0.033)
    
    cap.release()
    cv2.destroyAllWindows()
        
def MMPOSE_PRERUN_play_video_with_predictor(data, video_path):  
     
    predictor = PosePredictor(run_on_cpu=True)
   
    cap = cv2.VideoCapture(video_path)
   
    for frame_number in range(len(data)):
        rval, frame = cap.read()
        if not rval:
            break
        # predict from image
        prediction = data[frame_number]
        
        for point in prediction:
            cv2.circle(frame, (int(point[1]), int(point[2])), 3, (0, 255, 0), -1)
            print(point)

        
        # set rotation for all bones in frame
        MMPOSE_set_rotation_for_all_bones_in_frame(frame_number, prediction)
        
        cv2.imshow("Recording Animation", frame)
        if cv2.waitKey(33) == ord('q'):
            break
        
        # time.sleep(0.033)
    
    cap.release()
    cv2.destroyAllWindows()


# ======================================= MAIN PROGRAM HERE =======================================

# Set video path
def play_video_with_predictor(video_path = '../videos/curry_shooting.mp4', options = "onnx"):
    json_path = ''
    json_mp_path = ''
    if video_path == '../videos/yuchieh5.mp4':        
        json_mp_path = 'yuchieh5_mp.json'
        json_path = 'yuchieh5_mmpose.json'
    elif video_path == '../videos/curry_shooting.mp4':
        json_mp_path = 'curry_shooting_mp.json'
        json_path = 'curry_shooting_mmpose.json'
    elif video_path == '../videos/yuchieh5_1.mp4':
        json_mp_path = 'yuchieh5_1_mp.json'
        json_path = 'yuchieh5_1_mmpose.json'

    # play video with predictor
    if options == "onnx":
        MP_ONNX_play_video_with_predictor(video_path)
        
    elif options == "mp" or options == "mediapipe":
        with open(json_mp_path, 'r') as file:
            data = json.load(file)    
        frame_info = data['frame_info']
        keypoints = np.array(data['keypoints'])
        print(keypoints.shape)
        keypoints[:, :, 0] = keypoints[:, :, 0] * int(frame_info["width"]) 
        keypoints[:, :, 1] = keypoints[:, :, 1] * int(frame_info["height"]) 
        keypoints[:, :, 2] = keypoints[:, :, 2] * int(frame_info["width"])
        MP_PRERUN_play_video_with_predictor(keypoints, video_path)
    elif options == "mmpose" or options == "motionbert":
        
        with open(json_path, 'r') as file:
            data = json.load(file)    
        frame_info = data['frame_info']
        keypoints = np.array(data['keypoints'])
        print(keypoints.shape)
        MMPOSE_PRERUN_play_video_with_predictor(keypoints, video_path)

video_paths = ['../videos/yuchieh5.mp4', '../videos/curry_shooting.mp4', '../videos/yuchieh5_1.mp4']
options = ["onnx", "mediapipe", "motionbert"]


play_video_with_predictor(video_paths[1], options[2])



