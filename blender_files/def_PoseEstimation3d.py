import bpy
import numpy as np
import time

import subprocess
import sys
import os

# path to python.exe
python_exe = os.path.join(sys.prefix, 'bin', 'python.exe')
py_lib = os.path.join(sys.prefix, 'lib', 'site-packages','pip')

# install opencv
subprocess.call([python_exe, py_lib, "install", "opencv_python"])
# install mediapipe
subprocess.call([python_exe, py_lib, "install", "mediapipe"])

# blender_file_path = os.path.dirname(bpy.data.filepath)
# python_exec_path = 'C:\Program Files\Blender Foundation\Blender 3.0\3.0\python\Lib\site-packages'
# if not blender_file_path in sys.path:
#     sys.path.append(blender_file_path)
# if not python_exec_path in sys.path:
#     sys.path.append(python_exec_path)

import mmpose
import mediapipe
 
import imp
imp.reload(mmpose)
imp.reload(mediapipe)

from mmpose.apis import MMPoseInferencer
from mediapipe import Image, ImageFormat
from mediapipe.tasks import python
from mediapipe.tasks.python import vision


class PoseEstimation3d:

    def __init__(self):
        self.motionbert_inferencer = MMPoseInferencer(pose3d="motionbert_dstformer-ft-243frm_8xb32-120e_h36m", device="cuda")

        base_options = python.BaseOptions(
            model_asset_path='pose_landmarker.task',
        )
        options = vision.PoseLandmarkerOptions(
            base_options=base_options,
            output_segmentation_masks=True)

        self.mediapipe_detector = vision.PoseLandmarker.create_from_options(options)


    def estimate_pose_motionbert(self, image, time_inference):
        if time_inference:
            start_time = time.time()
            result = next(self.motionbert_inferencer(image))
            print(f"MotionBert Time taken: {time.time() - start_time} seconds")
        else:
            result = next(self.motionbert_inferencer(image))
        return np.hstack((result['predictions'][0][0]['keypoints'], np.array(result['predictions'][0][0]['keypoint_scores']).reshape(-1, 1)))

    def estimate_pose_mediapipe(self, image, time_inference):
        if time_inference:
            start_time = time.time()
            detection_result = self.mediapipe_detector.detect(Image(image_format=ImageFormat.SRGB, data=image))
            print(f"MediaPipe Time taken: {time.time() - start_time} seconds")
        else:
            detection_result = self.mediapipe_detector.detect(Image(image_format=ImageFormat.SRGB, data=image))
        detection_result = [[item.x, item.y, item.z, item.presence] for item in detection_result.pose_landmarks[0]]
        return np.array(detection_result)
        
    def estimate_pose(self, image, method="motionbert", time_inference=True):
        if method == "motionbert":
            return self.estimate_pose_motionbert(image, time_inference)
        elif method == "mediapipe":
            return self.estimate_pose_mediapipe(image, time_inference)
        else:
            raise ValueError("Invalid method")


