import bpy
import math
import sys
import os
import numpy as np
import json

dir = os.path.dirname(bpy.data.filepath)
if not dir in sys.path:
    sys.path.append(dir)

import def_Bone
import def_constants

# this next part forces a reload in case you edit the source after you first start the blender session
import imp
imp.reload(def_Bone)
imp.reload(def_constants)

from def_Bone import *
from def_constants import *

# ============================================= FUNCTIONS LIST HERE ========================================================

def convert_mediapipe_output_into_rotation_list(mediapipe_output):

    mediapipe_output = np.array(mediapipe_output)
    mediapipe_xyz = mediapipe_output[:, 0:3]

    # change to blender orientation
    rotate_x = [[1, 0, 0], [0, 0, 1], [0, -1, 0]]
    for i in range(0, len(mediapipe_xyz)):
        mediapipe_xyz[i] = np.dot(rotate_x, mediapipe_xyz[i])
        
    mediapipe_xyz = np.array(mediapipe_xyz)
    
    mediapipe_xyz[:, 1] *= 0.2
    
    # Set up shoulder_L_rotation
    shoulder_L_rotation = mediapipe_xyz[MP_ELBOW_L] - mediapipe_xyz[MP_SHOULDER_L]
    # normalize shoudler_L_rotation
    shoulder_L_rotation = shoulder_L_rotation / np.linalg.norm(shoulder_L_rotation)

    # Set up elbow_L_rotation
    elbow_L_rotation = mediapipe_xyz[MP_HAND_L] - mediapipe_xyz[MP_ELBOW_L]
    # normalize elbow_L_rotation
    elbow_L_rotation = elbow_L_rotation / np.linalg.norm(elbow_L_rotation)

    # Set up hand_L_rotation
    hand_L_midpoint = (mediapipe_xyz[MP_FINGER1_L] + mediapipe_xyz[MP_FINGER2_L]) / 2
    hand_L_rotation = hand_L_midpoint - mediapipe_xyz[MP_HAND_L]
    # normalize hand_L_rotation
    hand_L_rotation = hand_L_rotation / np.linalg.norm(hand_L_rotation)

    # Set up hips_L_rotation
    hips_L_rotation = mediapipe_xyz[MP_KNEES_L] - mediapipe_xyz[MP_HIPS_L]
    # normalize hips_L_rotation
    hips_L_rotation = hips_L_rotation / np.linalg.norm(hips_L_rotation)

    # Set up knee_L_rotation
    knee_L_rotation = mediapipe_xyz[MP_ANKLE_L] - mediapipe_xyz[MP_KNEES_L]
    # normalize knee_L_rotation
    knee_L_rotation = knee_L_rotation / np.linalg.norm(knee_L_rotation)

    # Set up foot_L_rotation
    foot_L_rotation = mediapipe_xyz[MP_TOE_L] - mediapipe_xyz[MP_ANKLE_L]
    # normalize foot_L_rotation
    foot_L_rotation = foot_L_rotation / np.linalg.norm(foot_L_rotation)

    # =======================================================================================================

    # Set up shoulder_L_rotation
    shoulder_R_rotation = mediapipe_xyz[MP_ELBOW_R] - mediapipe_xyz[MP_SHOULDER_R]
    # normalize shoudler_L_rotation
    shoulder_R_rotation = shoulder_R_rotation / np.linalg.norm(shoulder_R_rotation)

    # Set up elbow_L_rotation
    elbow_R_rotation = mediapipe_xyz[MP_HAND_R] - mediapipe_xyz[MP_ELBOW_R]
    # normalize elbow_L_rotation
    elbow_R_rotation = elbow_R_rotation / np.linalg.norm(elbow_R_rotation)

    # Set up hand_L_rotation
    hand_R_midpoint = (mediapipe_xyz[MP_FINGER1_R] + mediapipe_xyz[MP_FINGER2_R]) / 2
    hand_R_rotation = hand_R_midpoint - mediapipe_xyz[MP_HAND_R]
    # normalize hand_L_rotation
    hand_R_rotation = hand_R_rotation / np.linalg.norm(hand_R_rotation)

    # Set up hips_L_rotation
    hips_R_rotation = mediapipe_xyz[MP_KNEES_R] - mediapipe_xyz[MP_HIPS_R]
    # normalize hips_L_rotation
    hips_R_rotation = hips_R_rotation / np.linalg.norm(hips_R_rotation)

    # Set up knee_L_rotation
    knee_R_rotation = mediapipe_xyz[MP_ANKLE_R] - mediapipe_xyz[MP_KNEES_R]
    # normalize knee_L_rotation
    knee_R_rotation = knee_R_rotation / np.linalg.norm(knee_R_rotation)

    # Set up foot_L_rotation
    foot_R_rotation = mediapipe_xyz[MP_TOE_R] - mediapipe_xyz[MP_ANKLE_R]
    # normalize foot_L_rotation
    foot_R_rotation = foot_R_rotation / np.linalg.norm(foot_R_rotation)
    
    # Set up root_rotation
    spine_midpoint = (mediapipe_xyz[MP_HIPS_L] + mediapipe_xyz[MP_HIPS_R]) / 2
    spine3_midpoint = (mediapipe_xyz[MP_SHOULDER_L] + mediapipe_xyz[MP_SHOULDER_R]) / 2
    root_rotation = spine3_midpoint - spine_midpoint
    root_rotation = root_rotation / np.linalg.norm(root_rotation)
    
    return [shoulder_L_rotation, 
            elbow_L_rotation, 
            hand_L_rotation, 
            hips_L_rotation, 
            knee_L_rotation, 
            foot_L_rotation, 
            shoulder_R_rotation, 
            elbow_R_rotation, 
            hand_R_rotation, 
            hips_R_rotation, 
            knee_R_rotation, 
            foot_R_rotation,
            root_rotation
            ]

def set_rotation_for_single_bone(bone_name, vector):    
    # open bone class
    bone = Bone(bone_name=bone_name)
    
    # get y-axis of upper_arm.L
    bone.set_bone_align_to_world_vector(world_vector = vector)  
    
    # set bone rotation to current keyframe
    bone.insert_rotation_to_current_keyframe()

def MP_set_rotation_for_all_bones(mediapipe_output):
    def sigmoid(x):
        return 1 / (1 + math.exp(-x))
    def ease_in_out_lerp(a, b, t):
        # Cubic ease-in-ease-out
        t = t * t * (3 - 2 * t)
        return a + (b - a) * t
    
    rotation_list = convert_mediapipe_output_into_rotation_list(mediapipe_output)
    
    # set rotation of spines
    # set_rotation_for_single_bone(bone_name="spine", vector=rotation_list[MP_ROOT_ROTATION])
    # set_rotation_for_single_bone(bone_name="spine.001", vector=rotation_list[MP_ROOT_ROTATION])
    set_rotation_for_single_bone(bone_name="spine.002", vector=rotation_list[MP_ROOT_ROTATION])
    set_rotation_for_single_bone(bone_name="spine.003", vector=rotation_list[MP_ROOT_ROTATION])
    
    # rotate shoudler
    if sigmoid(mediapipe_output[MP_SHOULDER_L][3]) > 0.5:
        bone = Bone("shoulder.L")
        if rotation_list[MP_SHOULDER_L_ROTATION][2] > 0:
            bone.set_bone_rotation_around_world_space_vector([1, 0, 0], ease_in_out_lerp(0, 45, rotation_list[MP_SHOULDER_L_ROTATION][2]))
        elif rotation_list[MP_SHOULDER_L_ROTATION][2] > -0.2:
            bone.set_bone_rotation_around_world_space_vector([1, 0, 0], ease_in_out_lerp(0, -30, rotation_list[MP_SHOULDER_L_ROTATION][2] / 0.2))
        else:
            bone.set_bone_rotation_around_world_space_vector([1, 0, 0], -30)
            
    if sigmoid(mediapipe_output[MP_SHOULDER_R][3]) > 0.5:
        bone = Bone("shoulder.R")
        if rotation_list[MP_SHOULDER_R_ROTATION][2] > 0:
            bone.set_bone_rotation_around_world_space_vector([1, 0, 0], ease_in_out_lerp(0, 45, rotation_list[MP_SHOULDER_R_ROTATION][2]))
        elif rotation_list[MP_SHOULDER_R_ROTATION][2] > -0.2:
            bone.set_bone_rotation_around_world_space_vector([1, 0, 0], ease_in_out_lerp(0, -30, rotation_list[MP_SHOULDER_R_ROTATION][2] / 0.2))
        else:
            bone.set_bone_rotation_around_world_space_vector([1, 0, 0], -30)
    
    # align limbs to world vector
    if sigmoid(mediapipe_output[MP_SHOULDER_L][3]) > 0.5:
        set_rotation_for_single_bone(bone_name="upper_arm.L", vector=rotation_list[MP_SHOULDER_L_ROTATION])

    if sigmoid(mediapipe_output[MP_ELBOW_L][3]) > 0.5:
        set_rotation_for_single_bone(bone_name="forearm.L", vector=rotation_list[MP_ELBOW_L_ROTATION])

    if sigmoid(mediapipe_output[MP_HAND_L][3]) > 0.5:
        set_rotation_for_single_bone(bone_name="hand.L", vector=rotation_list[MP_HAND_L_ROTATION])

    if sigmoid(mediapipe_output[MP_HIPS_L][3]) > 0.5:
        set_rotation_for_single_bone(bone_name="thigh.L", vector=rotation_list[MP_HIPS_L_ROTATION])

    if sigmoid(mediapipe_output[MP_KNEES_L][3]) > 0.5:
        set_rotation_for_single_bone(bone_name="shin.L", vector=rotation_list[MP_KNEE_L_ROTATION])

    if sigmoid(mediapipe_output[MP_TOE_L][3]) > 0.5:
        set_rotation_for_single_bone(bone_name="foot.L", vector=rotation_list[MP_FOOT_L_ROTATION])

    if sigmoid(mediapipe_output[MP_SHOULDER_R][3]) > 0.5:
        set_rotation_for_single_bone(bone_name="upper_arm.R", vector=rotation_list[MP_SHOULDER_R_ROTATION])

    if sigmoid(mediapipe_output[MP_ELBOW_R][3]) > 0.5:
        set_rotation_for_single_bone(bone_name="forearm.R", vector=rotation_list[MP_ELBOW_R_ROTATION])

    if sigmoid(mediapipe_output[MP_HAND_R][3]) > 0.5:
        set_rotation_for_single_bone(bone_name="hand.R", vector=rotation_list[MP_HAND_R_ROTATION])

    if sigmoid(mediapipe_output[MP_HIPS_R][3]) > 0.5:
        set_rotation_for_single_bone(bone_name="thigh.R", vector=rotation_list[MP_HIPS_R_ROTATION])

    if sigmoid(mediapipe_output[MP_KNEES_R][3]) > 0.5:
        set_rotation_for_single_bone(bone_name="shin.R", vector=rotation_list[MP_KNEE_R_ROTATION])

    if sigmoid(mediapipe_output[MP_TOE_R][3]) > 0.5:
        set_rotation_for_single_bone(bone_name="foot.R", vector=rotation_list[MP_FOOT_R_ROTATION])

def MP_set_rotation_for_all_bones_in_frame(frame_number, mediapipe_output):
    # Enable auto keyframing
    bpy.context.scene.tool_settings.use_keyframe_insert_auto = True
    
    # Move to the specified frame
    bpy.context.scene.frame_set(frame_number)    
    
    # Set rotation for all bones
    MP_set_rotation_for_all_bones(mediapipe_output)
    
    # Update scene to create real time display
    bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)

# ==================== MMPOSE FUNCTIONS ====================
 
def convert_mmpose_output_into_rotation_list(mmpose_output):

    mmpose_output = np.array(mmpose_output)
    mmpose_xyz = mmpose_output[:, 0:3]
    
    # mmpose_xyz[:, 0], mmpose_xyz[:, 1] = mmpose_xyz[:, 1], mmpose_xyz[:, 0]
    
    # mmpose_xyz[:, 1] *= 0.5
    mmpose_xyz[:, 0] *= -1

    # change to blender orientation
    # rotate_x = [[1, 0, 0], [0, 0, 1], [0, -1, 0]]
    # for i in range(0, len(mmpose_xyz)):
    #     mmpose_xyz[i] = np.dot(rotate_x, mmpose_xyz[i])
        
    mmpose_xyz = np.array(mmpose_xyz)
    
    null_vector = np.array([0, 0, 0])
    
    # Set up shoulder_L_rotation
    shoulder_L_rotation = mmpose_xyz[MMPOSE_LEFT_ELBOW] - mmpose_xyz[MMPOSE_LEFT_SHOULDER]
    # normalize shoudler_L_rotation
    shoulder_L_rotation = shoulder_L_rotation / np.linalg.norm(shoulder_L_rotation)
    
    # Set up elbow_L_rotation
    elbow_L_rotation = mmpose_xyz[MMPOSE_LEFT_WRIST] - mmpose_xyz[MMPOSE_LEFT_ELBOW]
    # normalize elbow_L_rotation
    elbow_L_rotation = elbow_L_rotation / np.linalg.norm(elbow_L_rotation)


    # Set up hips_L_rotation
    hips_L_rotation = mmpose_xyz[MMPOSE_LEFT_KNEE] - mmpose_xyz[MMPOSE_LEFT_HIP]
    # normalize hips_L_rotation
    hips_L_rotation = hips_L_rotation / np.linalg.norm(hips_L_rotation)

    # Set up knee_L_rotation
    knee_L_rotation = mmpose_xyz[MMPOSE_LEFT_ANKLE] - mmpose_xyz[MMPOSE_LEFT_KNEE]
    # normalize knee_L_rotation
    knee_L_rotation = knee_L_rotation / np.linalg.norm(knee_L_rotation)


    # =======================================================================================================

    # Set up shoulder_L_rotation
    shoulder_R_rotation = mmpose_xyz[MMPOSE_RIGHT_ELBOW] - mmpose_xyz[MMPOSE_RIGHT_SHOULDER]
    # normalize shoudler_L_rotation
    shoulder_R_rotation = shoulder_R_rotation / np.linalg.norm(shoulder_R_rotation)

    # Set up elbow_L_rotation
    elbow_R_rotation = mmpose_xyz[MMPOSE_RIGHT_WRIST] - mmpose_xyz[MMPOSE_RIGHT_ELBOW]
    # normalize elbow_L_rotation
    elbow_R_rotation = elbow_R_rotation / np.linalg.norm(elbow_R_rotation)

    # Set up hips_L_rotation
    hips_R_rotation = mmpose_xyz[MMPOSE_RIGHT_KNEE] - mmpose_xyz[MMPOSE_RIGHT_HIP]
    # normalize hips_L_rotation
    hips_R_rotation = hips_R_rotation / np.linalg.norm(hips_R_rotation)

    # Set up knee_L_rotation
    knee_R_rotation = mmpose_xyz[MMPOSE_RIGHT_ANKLE] - mmpose_xyz[MMPOSE_RIGHT_KNEE]
    # normalize knee_L_rotation
    knee_R_rotation = knee_R_rotation / np.linalg.norm(knee_R_rotation)
    
    spine_rotation = mmpose_xyz[MMPOSE_SPINE] - mmpose_xyz[MMPOSE_PELVIS]
    spine_rotation = spine_rotation / np.linalg.norm(spine_rotation)
    
    spine3_rotation = mmpose_xyz[MMPOSE_THORAX_CHEST] - mmpose_xyz[MMPOSE_SPINE]
    spine3_rotation = spine3_rotation / np.linalg.norm(spine3_rotation)
    
    neck_rotation = mmpose_xyz[MMPOSE_NECK_NOSE] - mmpose_xyz[MMPOSE_THORAX_CHEST]
    neck_rotation = neck_rotation / np.linalg.norm(neck_rotation)
    
    mm_shoulder_L_rotation = mmpose_xyz[MMPOSE_LEFT_SHOULDER] - mmpose_xyz[MMPOSE_THORAX_CHEST]
    mm_shoulder_L_rotation = mm_shoulder_L_rotation / np.linalg.norm(mm_shoulder_L_rotation)
    
    mm_shoulder_R_rotation = mmpose_xyz[MMPOSE_RIGHT_SHOULDER] - mmpose_xyz[MMPOSE_THORAX_CHEST]
    mm_shoulder_R_rotation = mm_shoulder_R_rotation / np.linalg.norm(mm_shoulder_R_rotation)
    
    
    
    return [shoulder_L_rotation, 
            elbow_L_rotation, 
            null_vector, 
            hips_L_rotation, 
            knee_L_rotation, 
            null_vector, 
            shoulder_R_rotation, 
            elbow_R_rotation, 
            null_vector, 
            hips_R_rotation, 
            knee_R_rotation, 
            null_vector,
            null_vector,
            spine_rotation,
            spine3_rotation,
            neck_rotation,
            mm_shoulder_L_rotation,
            mm_shoulder_R_rotation
            ]
    
def MMPOSE_set_rotation_for_all_bones(mmpose_output):
    def sigmoid(x):
        return 1 / (1 + math.exp(-x))
    def lerp(a, b, t):
        return (1-t) * a + t * b
    
    rotation_list = convert_mmpose_output_into_rotation_list(mmpose_output)
    
    start_vector = rotation_list[MMPOSE_SPINE_ROTATION]
    end_vector = rotation_list[MMPOSE_SPINE3_ROTATION]
    
    if sigmoid(mmpose_output[MMPOSE_PELVIS][3]) > 0.5:
        set_rotation_for_single_bone(bone_name="spine", vector=lerp(start_vector, end_vector, 0))
        set_rotation_for_single_bone(bone_name="spine.001", vector=lerp(start_vector, end_vector, 1/3))
        set_rotation_for_single_bone(bone_name="spine.002", vector=lerp(start_vector, end_vector, 2/3))
        set_rotation_for_single_bone(bone_name="spine.003", vector=lerp(start_vector, end_vector, 1))
        
    if sigmoid(mmpose_output[MMPOSE_THORAX_CHEST][3]) > 0.5:
        set_rotation_for_single_bone(bone_name="spine.004", vector=rotation_list[MMPOSE_NECK_ROTATION])
        set_rotation_for_single_bone(bone_name="shoulder.L", vector=rotation_list[MMPOSE_SHOULDER_L_ROTATION])        
        set_rotation_for_single_bone(bone_name="shoulder.R", vector=rotation_list[MMPOSE_SHOULDER_R_ROTATION])
        
    
    
    # LIMBS
    
    # align limbs to world vector
    if sigmoid(mmpose_output[MMPOSE_LEFT_SHOULDER][3]) > 0.5:
        set_rotation_for_single_bone(bone_name="upper_arm.L", vector=rotation_list[MP_SHOULDER_L_ROTATION])

    if sigmoid(mmpose_output[MMPOSE_LEFT_ELBOW][3]) > 0.5:
        set_rotation_for_single_bone(bone_name="forearm.L", vector=rotation_list[MP_ELBOW_L_ROTATION])

    # if sigmoid(mmpose_output[MMPOSE_LEFT_WRIST][3]) > 0.5:
    #     set_rotation_for_single_bone(bone_name="hand.L", vector=rotation_list[MP_HAND_L_ROTATION])

    if sigmoid(mmpose_output[MMPOSE_LEFT_HIP][3]) > 0.5:
        set_rotation_for_single_bone(bone_name="thigh.L", vector=rotation_list[MP_HIPS_L_ROTATION])

    if sigmoid(mmpose_output[MMPOSE_LEFT_KNEE][3]) > 0.5:
        set_rotation_for_single_bone(bone_name="shin.L", vector=rotation_list[MP_KNEE_L_ROTATION])

    # if sigmoid(mmpose_output[MMPOSE_LEFT_ANKLE][3]) > 0.5:
    #     set_rotation_for_single_bone(bone_name="foot.L", vector=rotation_list[MP_FOOT_L_ROTATION])

    if sigmoid(mmpose_output[MMPOSE_RIGHT_SHOULDER][3]) > 0.5:
        set_rotation_for_single_bone(bone_name="upper_arm.R", vector=rotation_list[MP_SHOULDER_R_ROTATION])

    if sigmoid(mmpose_output[MMPOSE_RIGHT_ELBOW][3]) > 0.5:
        set_rotation_for_single_bone(bone_name="forearm.R", vector=rotation_list[MP_ELBOW_R_ROTATION])

    # if sigmoid(mmpose_output[MMPOSE_RIGHT_WRIST][3]) > 0.5:
    #     set_rotation_for_single_bone(bone_name="hand.R", vector=rotation_list[MP_HAND_R_ROTATION])

    if sigmoid(mmpose_output[MMPOSE_RIGHT_HIP][3]) > 0.5:
        set_rotation_for_single_bone(bone_name="thigh.R", vector=rotation_list[MP_HIPS_R_ROTATION])

    if sigmoid(mmpose_output[MMPOSE_RIGHT_KNEE][3]) > 0.5:
        set_rotation_for_single_bone(bone_name="shin.R", vector=rotation_list[MP_KNEE_R_ROTATION])

    # if sigmoid(mmpose_output[MMPOSE_RIGHT_ANKLE][3]) > 0.5:
    #     set_rotation_for_single_bone(bone_name="foot.R", vector=rotation_list[MP_FOOT_R_ROTATION])
      
def MMPOSE_set_rotation_for_all_bones_in_frame(frame_number, mmpose_output):
    # Enable auto keyframing
    bpy.context.scene.tool_settings.use_keyframe_insert_auto = True
    
    # Move to the specified frame
    bpy.context.scene.frame_set(frame_number)    
    
    # Set rotation for all bones
    MMPOSE_set_rotation_for_all_bones(mmpose_output)
    
    # Update scene to create real time display
    bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
