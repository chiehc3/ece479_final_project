import bpy
import numpy as np
import json

import subprocess
import sys
import os

#python_exe = os.path.join(sys.prefix, 'bin', 'python.exe')
#py_lib = os.path.join(sys.prefix, 'lib', 'site-packages','pip')

#subprocess.call([python_exe, py_lib, "install", "opencv-python"])
#subprocess.call([python_exe, py_lib, "install", "mediapipe"])

subprocess.check_call([sys.executable, "-m", "ensurepip"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "--upgrade", "pip"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "mediapipe"])
subprocess.check_call([sys.executable, "-m", "pip", "install", "opencv-python"])

try:
    
    import mediapipe as mp
    import cv2
except Exception as e:
    print("ouch")
        # bpy.ops.message.messagebox('INVOKE_DEFAULT', message = 'Installing additional libraries, this may take a moment...')
    bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
    import cv2
    import mediapipe as mp

dir = os.path.dirname(bpy.data.filepath)
if not dir in sys.path:
    sys.path.append(dir)
    
import def_PosePredictor 
import def_PoseEstimation3d
import lib_rotation

import imp
imp.reload(def_PosePredictor)
imp.reload(def_PoseEstimation3d)
imp.reload(lib_rotation)

from lib_rotation import *
from def_PosePredictor import PosePredictor
from def_PoseEstimation3d import PoseEstimation3d

# =============================================== CODE STARTS HERE ===============================================

def MP_API_play_video_with_predictor(video_path):
    model = PoseEstimation3d()
    
    cap = cv2.VideoCapture(video_path)
    verts = []
    frame_number = 0
    
    if not cap.isOpened():
        print("Error: Could not open video.")
        return

    while True:
        rval, frame = cap.read()
        if not rval:
            break
        
        # predict from image
        result = model.estimate_pose(frame, method="mediapipe", time_inference=False)
        
        # add prediction to list
        verts.append(result)
        
        # draw indicator dots
        # frame = predictor.draw_predicted_dots_on_image(frame)
        
        # set rotation for all bones in frame
        MP_set_rotation_for_all_bones_in_frame(frame_number, result)
        frame_number += 1
        
        cv2.imshow("Recording Animation", frame)
        if cv2.waitKey(33) == ord('q'):
            break
    
    cap.release()
    cv2.destroyAllWindows()

    verts_array = np.array(verts)
    vertices_list = verts_array.tolist()
    json_data = json.dumps(vertices_list, indent=4)
    with open('vertices.json', 'w') as f:
        json.dump(vertices_list, f)

    print(len(verts_array))
    


# Set video path
video_path = '../videos/curry_shooting.mp4'

# play video with predictor
MP_API_play_video_with_predictor(video_path)

