import torch
import torch.onnx
from A2J import A2J_model  # Make sure to import your model's class correctly

# Load the trained model from file
trained_model = A2J_model(15)
trained_model.load_state_dict(torch.load(r'models/ITOP_side.pth', map_location=torch.device('cpu')))

# Set the model to evaluate mode
trained_model.eval()

# Create a dummy input that matches the input size of the model
# For example, if your model expects a 1x3x224x224 input, create a tensor of that size
dummy_input = torch.randn(1, 3, 224, 224)  # Adjust the size according to your model

# Specify the path for the output ONNX file
output_onnx_file = 'itop_side2.onnx'

# Export the model
torch.onnx.export(trained_model,         # model being run
                  dummy_input,           # model input (or a tuple for multiple inputs)
                  output_onnx_file,      # where to save the model
                  export_params=True,    # store the trained parameter weights inside the model file
                  opset_version=10,      # the ONNX version to export the model to
                  do_constant_folding=True,  # whether to execute constant folding for optimization
                  input_names=['input'],    # the model's input names
                  output_names=['output'],  # the model's output names
                  dynamic_axes={'input': {0: 'batch_size'},  # variable length axes
                                'output': {0: 'batch_size'}})

print(f'Model has been converted to ONNX and saved to {output_onnx_file}')